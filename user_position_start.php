<?php
//user_position.php
require_once('database.php');


if(isset($_POST["start_x"]) && isset($_POST["start_y"]) )
    {
        $start_x =intval(mysqli_real_escape_string($connect, $_POST["start_x"]));
        $start_y =intval(mysqli_real_escape_string($connect, $_POST["start_y"]));
        // $arrival_x = intval(mysqli_real_escape_string($connect, $_POST["arrival_x"]));
        // $arrival_y = intval(mysqli_real_escape_string($connect, $_POST["arrival_y"]));

        $request = "SELECT *, round(sqrt( (abs(position_x - $start_x)*abs(position_x - $start_x))+ (abs(position_y - $start_y)*abs(position_y - $start_y))),2) AS distance FROM positions_stations ORDER BY distance LIMIT 2";
        $result = mysqli_query($connect, $request);
        if(mysqli_num_rows($result) > 0) {
       
            while($row = mysqli_fetch_array($result) ){
                $_SESSION["start_x"] = $row["position_x"];
                $_SESSION["start_y"] = $row["position_y"];
    
                $name_station = $row["name_station"];
                $distance = $row["distance"];
                echo"<tr><td id='station_start'>".$name_station."</td><td>" .$distance."</td></tr>";
            }
        } else {
            echo"<p>Error</p> ";
        }
    }
