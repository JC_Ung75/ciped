$(document).ready(function () {
  // au click sur button avec id login
  $("#login").click(function () {
    // variable email et password prennent la valeur de leur champs input respectifs
    let email = $("#email").val();
    let password = $("#password").val();
    // trim permet de supprimer les espaces en debut et fin
    // si la longueur de email et password est superieur à 0 alors
    if ($.trim(email).length > 0 && $.trim(password).length > 0) {
      // appel fonction $.ajax ,permet de faire appel ajax en jquery
      $.ajax({
        // fichier cible
        url: "login.php",
        // method
        method: "POST",
        // données envoyées
        data: { email: email, password: password },
        // ne met pas les données en memoire cache
        cache: false,
        // avant envoie lance la fonction qui change la valeur du bouton en connecting...
        beforeSend: function () {
          $("#login").val("connecting...");
        },
        // en cas de succes
        success: function (data) {
          // s'il y a des resultats on charge la page home.php
          if (data) {
            // $("body").load("home.php").hide().fadeIn(1500);
            $(".container_login").hide();
            $(".navbar").show();
            $("h4").html(data);
            $(".container_from").show();
            $(".container_to").show();
          }
          // sinon message d'erreur avec un effet secousse sur le container
          else {
            let options = {
              distance: "40",
              direction: "left",
              times: "3",
            };
            //$("#box").effect("shake", options, 800);
            $("#login").val("Login");
            $("#error").html("Invalid email or Password");
          }
        },
      });
    }
  }); /// fin login

  // debut choix station départ
  $("#btn_input_from").click(function () {
    let start_x = $("#start_x").val();
    let start_y = $("#start_y").val();
    console.log(start_x, start_y);
    if ($.trim(start_x).length > 0 && $.trim(start_y).length > 0) {
      $.ajax({
        // fichier cible
        url: "user_position_start.php",
        // methode
        method: "POST",
        // données envoyées
        data: { start_x: start_x, start_y: start_y },
        // ne met pas les données en memoire cache
        cache: false,

        // en cas de reussite
        success: function (data) {
          // s'il y a des resultats on charge la page user_position.php dans la div near_station
          if (data) {
            // $(".near_station_from").html(data);
            $("#table_near_station_from tbody").empty();
            $("#table_near_station_from ").append(data);

            $(".proposal_station_from").show();
            $("body").on("click", "#station_start", function () {
              //console.log($(this).text());
              let $this = $(this);
              let station_pick = $this.text();
              console.log(station_pick);

              $.ajax({
                // fichier cible
                url: "available_bikes.php",
                // methode
                method: "POST",
                // données envoyées
                data: { station_pick: station_pick },
                // info stockés dans la memoire cache
                cache: false,
                // en cas de réussite
                success: function (data2) {
                  $("#table_bikes_available tbody").empty();
                  $("#table_bikes_available").append(data2);
                  $(".free_bikes").show();
                  $("body").on("click", "#sn_bike", function () {
                    let $this2 = $(this);
                    let sn_bike = $this2.text();
                    console.log(sn_bike);
                    $.ajax({
                      // fichier cible
                      url: "reservation_start.php",
                      // methode
                      method: "POST",
                      // données envoyées
                      data: { sn_bike: sn_bike },
                      // info stockés dans la memoire cache
                      cache: false,
                      // en cas de réussite
                      success: function (data3) {
                        $("#table_resa_start tbody").empty();
                        $("#table_resa_start").append(data3);
                        $("#table_resa_start").show();
                        $(".container_reservation").show();
                      },
                    });
                  });
                },
              });
            });
          }
          // sinon message d'erreur
          else {
          }
        },
      });
    }
  }); /// fin click btn_input_from

  // debut choix station arrivée
  $("#btn_input_to").click(function () {
    let arrival_x = $("#arrival_x").val();
    let arrival_y = $("#arrival_y").val();
    console.log(arrival_x, arrival_y);
    if ($.trim(arrival_x).length > 0 && $.trim(arrival_y).length > 0) {
      $.ajax({
        // fichier cible
        url: "user_position_arrival.php",
        // method
        method: "POST",
        // données envoyées
        data: { arrival_x: arrival_x, arrival_y: arrival_y },
        // ne met pas les données en memoire cache
        cache: false,

        // en cas de reussite
        success: function (data) {
          // s'il y a des resultats on charge la page user_position.php dans la div near_station
          if (data) {
            // $(".near_station_to").html(data);
            $("#table_near_station_to tbody").empty();
            $("#table_near_station_to").append(data);
            $(".proposal_station_to").show();
            $("body").on("click", "#station_arrival", function () {
              let $this = $(this);
              let station_pick_arrival = $this.text();
              console.log(station_pick_arrival);

              $.ajax({
                // fichier cible
                url: "available_bornes.php",
                // methode
                method: "POST",
                // données envoyées
                data: { station_pick_arrival: station_pick_arrival },
                // info stockés dans la memoire cache
                cache: false,
                // en cas de réussite
                success: function (data2) {
                  $("#table_bornes_available tbody").empty();
                  $("#table_bornes_available").append(data2);
                  $(".free_bornes").show();
                  $("body").on("click", "#click_borne", function () {
                    let $this3 = $(this);
                    let arrival_borne = $this3.text();
                    console.log(arrival_borne);

                    $.ajax({
                      // fichier cible
                      url: "reservation_arrival.php",
                      // methode
                      method: "POST",
                      // données envoyées
                      data: { arrival_borne: arrival_borne },
                      // info stockés dans la memoire cache
                      cache: false,
                      // en cas de réussite
                      success: function (data4) {
                        $("#table_resa_arrival tbody").empty();
                        $("#table_resa_arrival").append(data4);
                        $("#table_resa_arrival").show();
                        $(".container_reservation").show();
                        $(".cb").show();
                        $("body").on(
                          "click",
                          "#reservation_submit",
                          function () {
                            let email_reservation = $(
                              "#reservation_email"
                            ).val();
                            let cb_reservation = $("#reservation_cb").val();
                            console.log(email_reservation, cb_reservation);

                            $.ajax({
                              // fichier cible
                              url: "pay.php",
                              // methode
                              method: "POST",
                              // données envoyées
                              data: {
                                email_reservation: email_reservation,
                                cb_reservation: cb_reservation,
                              },
                              // info stockés dans la memoire cache
                              cache: false,
                              // en cas de réussite
                              beforeSend: function () {
                                $("#reservation_submit").val(
                                  "Traitement en cours..."
                                );
                              },
                              success: function (data5) {
                                $(".container_from").hide();
                                $(".container_to").hide();
                                $(".proposal_station_from").hide();
                                $(".proposal_station_to").hide();
                                $(".free_bikes").hide();
                                $(".free_bornes").hide();
                                $(".container_reservation").hide();
                                $(".confirmation").show();
                                $(".confirmation_ul").append(data5);
                              },
                            });
                          }
                        );
                      },
                    }); // fin ajax selection reservation borne arrivée
                  });
                },
              }); // fin ajax selection station arrivée
            });
          }
          // sinon message d'erreur
          else {
          }
        },
      });
    }
  }); /// fin click btn_input_to
}); // fin document ready
