<?php
//pay.php
require_once('database.php');

if( isset($_POST["email_reservation"]) && isset($_POST["cb_reservation"]) ){
    $email_reservation = mysqli_real_escape_string($connect, $_POST["email_reservation"]);
    $cb_reservation = mysqli_real_escape_string($connect, $_POST["cb_reservation"]);
    $station_name_start = $_SESSION["station_name_start"];
    $borne_id_start = $_SESSION["borne_id_start"];
    $bike_sn = $_SESSION["bike_sn"];
    $station_name_arrival = $_SESSION["station_name_arrival"];
    $borne_id_arrival = $_SESSION["borne_id_arrival"];
    // verification que la borne est bien libre avant restitution vélo 
    $request = "SELECT bornes.status FROM `bornes` WHERE bornes.id = '".$borne_id_arrival."'";
    $result = mysqli_query($connect, $request);
    if(mysqli_num_rows($result) > 0) {
        while($row = mysqli_fetch_array($result) ){
            $check_borne = $row["status"];
            // si $check_borne = libre, alors requete pour ajouter numero de vélo et mettre le statut de borne à occuper à la ligne bornes.id = $borne_id_arrival 
            if($check_borne == "libre"){
                $request2 = "UPDATE `bornes` SET `fk_bike` = '".$bike_sn."', `status` = 'occuper' WHERE `bornes`.`id` = '".$borne_id_arrival."' ";
                $result2 = mysqli_query($connect, $request2);
                // si mise à jour alors, mise de jour de la borne de depart 
                if($connect->query($request2)===TRUE){
                    $request3 = "UPDATE `bornes` SET `fk_bike` = '51', `status` = 'libre' WHERE `bornes`.`id` = '".$borne_id_start."' ";
                    $result3 = mysqli_query($connect, $request3);
                    if($connect->query($request3)==TRUE){
                        echo "<li>Station de départ : ".$station_name_start."</li>";
                        echo "<li>Borne Départ : ".$borne_id_start."</li>";
                        echo "<li>Vélo Numéro : ".$bike_sn."</li>";
                        echo "<li>***</li>";
                        echo "<li>Station d'arrivée : ".$station_name_arrival."</li>";
                        echo "<li>Borne Numéro : ".$borne_id_arrival."</li>";
                        echo "<li>***</li>";
                        echo "<li>Code Vélo  : ".rand(1000, 9999)."</li>";
                        echo "<li>Merci pour votre réservation : ".$_SESSION["firstname"]."</li>";
                    }
                }
            }
        }    
    } else {
        echo"<p>error! ".$check_borne."</p> ";
    }

}