<?php
//available_bikes.php
require_once('database.php');


if( isset($_POST["station_pick"]) )
    {
        $station_pick = mysqli_real_escape_string($connect, $_POST["station_pick"]);
        $_SESSION["station_pick"] = $station_pick;
        $request = "SELECT  station.name, station.bornes_id, bornes.id, bornes.fk_bike, bornes.status AS bstatus, bike.id_serial, bike.serial_number, bike.status AS bk_status
                    FROM  `station` 
		            INNER JOIN bornes 
                    ON station.bornes_id = bornes.id 
                    INNER JOIN bike
                    ON bornes.fk_bike = bike.id_serial
                    WHERE station.name = '".$station_pick."'
                    AND bike.status = 'Disponible'
                    LIMIT 5";
                    

        $result = mysqli_query($connect, $request);
        if(mysqli_num_rows($result) > 0) {
            
            while($row = mysqli_fetch_array($result) ){
                $borne_id = $row["bornes_id"];
                $status = $row["bstatus"];
                $bike_serial = $row["id_serial"];
                $bike_sn = $row["serial_number"];

                echo"<tr><td> ".$borne_id." </td><td> ".$status." </td><td id='sn_bike'> ".$bike_serial." </td></tr>";
            }
        } else {
            echo"<p>No result !</p> ";
        }
    }
 