<?php
//index.php
session_start();
// si la variable $_SESSION["email"] existe alors redirection sur page home.php (deja loggé)

if(isset($_SESSION["email"]))
{
 header("location:home.php");
}
?>

<html>
 <head>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <meta charset="utf-8">
  <title>CIPED Bike Rent</title>
  <script src="http://code.jquery.com/jquery-2.1.1.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Alegreya:ital,wght@1,400;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
  <script type="text/javascript" src="script.js"></script>
</head>
    <body>
        <div class="form_container">
            <h1>Créer un Compte</h1>
            <div id="box_register">            
                <form action="signup.php" method="post"  autocomplete="none">
                    <table>
                        <tr><td>First Name</td></tr>
                        <tr><td> <input type="text" name="first_name" id="firstname" class="form-control" required/></td></tr>
                        <tr><td>Email</td></tr>
                        <tr><td> <input type="email" name="email" id="email" class="form-control" required /></td></tr>
                        <tr><td>Password</td</tr>
                        <tr><td><input type="password" name="password" id="password" class="form-control"  autocomplete="new-password"/></td></tr>
                        <tr><td>Confirm Password</td></tr>
                        <tr><td><input type="password" name="confirm_password" id="confirm_password" class="form-control"  autocomplete="new-password"/></td></tr>
                        <tr><td id="error"><?= $_SESSION['message'] ?></td></tr>
                        <tr><td></td></tr>
                        <tr><td><input type="submit" name="register" id="register" class="btn_log" value="register" /></td></tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>