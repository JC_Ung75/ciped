<?php
//index.php
session_start();
// si la variable $_SESSION["email"] existe alors redirection sur page home.php (deja loggé)
if(isset($_SESSION["email"]))
{
 header("location:home.php");
}
?>
<html>
 <head>
  <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
  <meta charset="utf-8">
  <title>CIPED Bike Rent</title>
  <script src="http://code.jquery.com/jquery-2.1.1.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css2?family=Alegreya:ital,wght@1,400;1,900&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="style.css">
  <script type="text/javascript" src="script.js"></script>
</head>
<body>

<!-- debut partie login -->
    <div class="container_login">
        <h1>CIPED Bike Rent</h1><br /><br />
            <div id="box">
                    <form method="post">
                        <table>
                            <tr><td>Email</td></tr>
                            <tr><td> <input type="text" name="email" id="email" class="form-control" /></td></tr>
                            <tr><td>Password</td</tr>
                            <tr><td></td></tr>
                            <tr><td><input type="password" name="password" id="password" class="form-control"  autocomplete="none"/></td></tr>
                            <tr><td id="error"></td></tr>
                            <tr><td></td></tr> 
                            <tr><td><input type="button" name="login" id="login" class="btn_log" value="Login" /></td></tr>
                            <tr><td></td></tr> 
                            <tr><td></td></tr> 
                            <tr><td><a href="register.php">Créer un compte</a> </td></tr>
                        </table>
                    </form>

                
            </div>
    </div>
<!-- fin partie login -->

<!-- container principal -->

<div class="grid-container">
    
    <div class="navbar">
        <div id="navbar_a">
            <h4></h4> 
            <h3>Ciped - BikeRent</h3>
             <a href="logout.php">Logout</a>
        </div>
    </div>

    <div class="container_from">
        <div class="start">
            <h3>coordonnées de départ</h3>
            <label>Coordonnées de départ X</label>
            <input type="number" id="start_x">
            <label>Coordonnées de départ Y</label>
            <input type="number" id="start_y">
        </div>
        <input type="button" name="input_from" id="btn_input_from"  value="Valider" />
    </div>

    <div class="container_to">
        <div class="arrival">
            <h3>coordonnées d'arrivée</h3>
            <label>Coordonnées d'arrivée X</label>
            <input type="number" id="arrival_x">
            <label>Coordonnées d'arrivée Y</label>
            <input type="number" id="arrival_y">
        </div>
        <input type="button" name="input_to" id="btn_input_to"  value="Valider" />
    </div>

    <div class="proposal_station_from">
        <h3>Stations de départ </h3>
        <div class="near_station_from">
            <table id="table_near_station_from">
                <thead>
                    <tr><th>Station</th><th>Distance</th></tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="proposal_station_to">
        <h3>Stations d'arrivée </h3>
        <div class="near_station_to">
            <table id="table_near_station_to">
                <thead>
                    <tr><th>Station</th><th>Distance</th></tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <div class="free_bikes">
        <h3>Vélos disponibles</h3>      
        <div id="bikes_available">  
            <table id="table_bikes_available">
                <thead>
                    <tr><th>Numero Borne</th><th>Statut Borne</th><th>Numéro Vélo</th></tr>
                </thead>
                <tbody>
                </tbody>
            </table>
               
        </div>
    </div>

    <div class="free_bornes">
        <h3>Bornes disponibles</h3>      
        <div id="bornes_available">  
            <table id="table_bornes_available">
                <thead>
                    <tr><th>Numero Borne</th><th>Statut Borne</th></tr>
                </thead>
                <tbody>
                </tbody>
            </table>
               
        </div>
    </div>
    

    <div class="container_reservation">
        <div class="div_reservation">
            <h3>Réservation</h3>

            <div id="resa_start">
                <table id="table_resa_start">
                    <thead>
                        <tr>
                            <th colspan="2">Départ</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>

            <div id="resa_arrival">
                <table id="table_resa_arrival">
                    <thead>
                        <tr>
                            <th colspan="2">Arrivée</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="cb">
            <h3>Paiement</h3>
            <form method="post">
                <table id="table_cb">
                    <tr><td>Entrez votre Email:</td></tr>
                    <tr><td> <input type="email" name="reservation_email" id="reservation_email" required></td></tr>
                    <tr><td>Entrez votre numero de CB</td></tr>
                    <tr><td><input type="number" name="reservation_cb" id="reservation_cb" required></td></tr>
                    <tr><td><input type="buuton" value="Réserver" id="reservation_submit"></td></tr>
                </table>
            </form>
        </div>
    </div>

</div>
<div class="confirmation">
    <h3>Confirmation Réservation</h3>
    <ul class="confirmation_ul"></ul>
</div>

</body>

</html>

